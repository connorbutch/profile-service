#!/bin/bash
file="../$1/samconfig.toml"
#
if [ -f "$file" ]
then
  while IFS='=' read -r key value
  do
    key=$(echo $key | tr '.' '_')

    eval ${key}=\${value} 2>/dev/null || :
  done < "$file"
  regionWithoutQuotes=$(eval echo $region)
  stackNameWithoutQuotes=$(eval echo $stack_name)
  echo "stack name: $stackNameWithoutQuotes"
  baseUrl=`aws cloudformation describe-stacks --stack-name ${stackNameWithoutQuotes} --region ${regionWithoutQuotes} --query "Stacks[0].Outputs[?OutputKey=='UpdateProfileUrl'].OutputValue" --output text`
  export BASE_URL=$baseUrl
else
  echo "$file not found."
fi
npx artillery run loadTest.yaml