# Load Testing
## Purpose
This module exists to execute a brief load test (which won't cost you more than a nickel) to compare the latencies
of the two approaches:
- api gateway -> lambda -> sns 
- api gateway -> sns

## Execution
To run the load test, go to the loadTests.yaml file and replace the target value with the output from your
cloudformation stack you want to load test