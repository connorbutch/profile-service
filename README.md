# profile-service

This project demonstrates how to use api gateway service integrations to connect api gateway directly to sns (as well as api gateway to lambda to sns).  It then offers a few tips on how to use each implementation, and offers suggestions on when to use each technique.

This goes along with an article on medium (I will include the link here once it is published).

# Setup

There are two separate stacks inside this repo -- one showing the direct api gateway to sns integration, and one showing api gateway to lambda to sns.  Each exists in their own subfolder and can be deployed as separate using aws sam.

# Status
I do not plan on doing much (if any) more work to this repo, as it serves the purposes of the demo and article.

