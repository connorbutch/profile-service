const { SNSClient, PublishCommand } = require("@aws-sdk/client-sns");
const {v4} = require('uuid');

const MIN_PROFILE_ID_LENGTH = 1
const MAX_PROFILE_ID_LENGTH = 32

const MIN_FIRST_NAME_LENGTH = 1
const MAX_FIRST_NAME_LENGTH = 32

const MIN_LAST_NAME_LENGTH = 1
const MAX_LAST_NAME_LENGTH = 32

const MIN_USERNAME_LENGTH = 1
const MAX_USERNAME_LENGTH = 32

const topicArn = process.env.TOPIC_ARN;
const snsClient = new SNSClient({region: process.env.AWS_REGION});

exports.handler = async (event, context) => {
        const correlationId = v4();
        console.log(correlationId);
        const errorsWithRequest = validateRequest(event);
        if(errorsWithRequest.length > 0){
                    return {
                        'statusCode': 400,
                        'body': JSON.stringify(errorsWithRequest),
                        'headers': {'x-correlation-id': correlationId}
                    }
        }
        const body = JSON.parse(event.body);
        body['profileId'] = event.pathParameters.profileId;
        const publishCommand = new PublishCommand({
            Message: event.body,
            TopicArn: topicArn,
            MessageAttributes: {
                "TYPE": {
                    "DataType": "String",
                    "StringValue": "PROFILE_UPDATE_STARTED",
                    "BinaryValue": null
                }
            }
        });
        const result = await snsClient.send(publishCommand);
        return {
            'statusCode': 202,
            'body': JSON.stringify(body),
            'headers': {'x-correlation-id': correlationId}
        }
};

function validateRequest(event){
    const errors = [];
    if(event.pathParameters == null || event.pathParameters.profileId == undefined || event.pathParameters.profileId.length < MIN_PROFILE_ID_LENGTH || event.pathParameters.profileId.length > MAX_PROFILE_ID_LENGTH){
        errors.push(`profileId is required as the last part of the path and must have a length between ${MIN_PROFILE_ID_LENGTH} and ${MAX_PROFILE_ID_LENGTH} characters.`)
    }
    const bodyObject = JSON.parse(event.body);
    if(bodyObject == null){
        errors.push("Request body is required");
    }else{
        if(bodyObject.firstName == undefined || bodyObject.firstName.length < MIN_FIRST_NAME_LENGTH || bodyObject.firstName.length > MAX_FIRST_NAME_LENGTH){
            errors.push(`firstName field in the body of the request is required and must be have a length between ${MIN_FIRST_NAME_LENGTH} and ${MAX_FIRST_NAME_LENGTH} characters.`);
        }
        if(bodyObject.lastName == undefined || bodyObject.lastName.length < MIN_LAST_NAME_LENGTH || bodyObject.lastName.length > MAX_LAST_NAME_LENGTH){
            errors.push(`lastName field in the body of the request is required and must be have a length between ${MIN_LAST_NAME_LENGTH} and ${MAX_LAST_NAME_LENGTH} characters.`);
        }
        if(bodyObject.username == undefined || bodyObject.username.length < MIN_USERNAME_LENGTH || bodyObject.username.length > MAX_USERNAME_LENGTH){
            errors.push(`username field in the body of the request is required and must be have a length between ${MIN_USERNAME_LENGTH} and ${MAX_USERNAME_LENGTH} characters.`);
        }
    }
return errors;
}